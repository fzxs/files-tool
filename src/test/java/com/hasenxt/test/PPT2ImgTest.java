package com.hasenxt.test;

import com.hasnext.tool.poi.Offices;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;

public class PPT2ImgTest {

    @Test
    public void test01() throws IOException {
        Offices ppt = new Offices();


        ppt.ppt2Image(Path.of(Objects.requireNonNull(getClass().getResource("/")).getPath(), "test.pptx"), Path.of("/tmp/test"));
    }
}

package com.hasnext.tool.poi;

import com.hasnext.tool.img.Images;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hslf.usermodel.HSLFSlide;
import org.apache.poi.hslf.usermodel.HSLFSlideShow;
import org.apache.poi.hslf.usermodel.HSLFTextShape;
import org.apache.poi.sl.usermodel.Slide;
import org.apache.poi.sl.usermodel.SlideShow;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFTextShape;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

@Slf4j
public class Offices {

    @Setter
    private BiConsumer<Integer, Integer> process;

    public void ppt2OneImage(File pptFile, File save) throws Exception {

        ppt2OneImage(pptFile.toPath(), save.toPath());
    }

    public void ppt2OneImage(Path pptPath, Path save) throws Exception {

        Path out = Path.of(System.getProperty("java.io.tmpdir"), UUID.randomUUID().toString());

        out.toFile().mkdirs();

        System.out.println("临时路径:" + out);

        ppt2Image(pptPath, out);

        Images img = new Images();


        img.merge(out, save);

    }

    private Function<Path, SlideShow> slideShowFun = (path) -> {

        try (InputStream in = Files.newInputStream(path)) {

            if (path.toString().endsWith(".pptx")) {

                return new XMLSlideShow(in);
            } else {

                return new HSLFSlideShow(in);
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    };

    private Consumer<XSLFSlide> pptxFont = (s) -> {
        s.getShapes()
                .stream()
                .filter(xs -> xs instanceof XSLFTextShape)
                .map(xs -> (XSLFTextShape) xs)
                .forEach(xs -> {
                    System.out.println("转换元素...");
                    xs.getTextParagraphs().forEach(xt -> {
                        xt.getTextRuns().forEach(tr -> {
                            System.out.println("设置字体.");
                            tr.setFontFamily("微软雅黑");
                        });
                    });
                });
    };

    private Consumer<HSLFSlide> pptFont = (s) -> {
        s.getShapes()
                .stream()
                .filter(xs -> xs instanceof HSLFTextShape)
                .map(xs -> (HSLFTextShape) xs)
                .forEach(xs -> {
                    System.out.println("转换元素...");
                    xs.getTextParagraphs().forEach(xt -> {
                        xt.getTextRuns().forEach(tr -> {
                            System.out.println("设置字体.");
                            tr.setFontFamily("微软雅黑");
                        });
                    });
                });
    };

    private Function<Path, Consumer> setFont = (p) -> p.toString().endsWith(".pptx") ? pptxFont : pptFont;


    public void ppt2Image(Path pptPath, Path saveDir) throws IOException {


        SlideShow ppt = slideShowFun.apply(pptPath);

        Dimension size = ppt.getPageSize();

        List<Slide> slides = ppt.getSlides();

        for (int i = 0; i < slides.size(); i++) {

            if (Objects.nonNull(process)) {
                process.accept(i, slides.size());
            }
            var s = slides.get(i);

            System.out.println("当前页:" + i);

            setFont.apply(pptPath).accept(s);

            BufferedImage img = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);

            Graphics2D graphics = img.createGraphics();

            graphics.setPaint(Color.white);

            graphics.fill(new Rectangle2D.Float(0, 0, size.width, size.height));

            s.draw(graphics);

            String filename = "page_" + (i + 1) + ".png";

            try (OutputStream out = Files.newOutputStream(saveDir.resolve(filename));) {

                ImageIO.write(img, "png", out);
            }

        }
    }

}

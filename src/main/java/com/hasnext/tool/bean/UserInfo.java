package com.hasnext.tool.bean;

import lombok.Data;

@Data
public class UserInfo {

    private String name;

    private String age;

    private String sex;

    private String university;

    private String tel;

}

package com.hasnext.tool;

import com.hasnext.tool.poi.Offices;
import com.hasnext.tool.ui.MainTool;

import javax.swing.*;

public class Main {
    public static void main(String[] args) throws Exception {


        Offices ppt = new Offices();

//        args = new String[]{"/home/fzxs/workspace/files-tool/src/test/resources/test.pptx", "/tmp/aaa.png"};

        JFrame jf = new JFrame("PPT转图片");
        jf.setSize(400, 400);
        jf.setVisible(true);

        MainTool tool = new MainTool();

        jf.add(tool.$$$getRootComponent$$$());

        jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        tool.setRunAction(t -> {
            try {

                ppt.setProcess((val, total) -> {
                    System.out.println("======进度==>" + val + "/" + total);
                    t.getProgressBar().setValue(val + 1);
                    t.getProgressBar().setMaximum(total);
                    t.getProgressBar().setStringPainted(true);
                    t.getProgressBar().update(t.getProgressBar().getGraphics());

                });
                ppt.ppt2OneImage(t.getPptFile(), t.getSaveFile());

            } catch (Exception e) {

                e.printStackTrace();
            }
        });


    }
}

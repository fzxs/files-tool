package com.hasnext.tool.img;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class Images {


    /**
     * 将一组图片合成一张
     *
     * @param img1
     * @param img2
     * @param isHorizontal
     * @return
     * @throws IOException
     */
    private BufferedImage mergeImage(BufferedImage img1, BufferedImage img2, boolean isHorizontal) throws IOException {
        int w1 = img1.getWidth();
        int h1 = img1.getHeight();
        int w2 = img2.getWidth();
        int h2 = img2.getHeight();
// 从图片中读取RGB
        int[] ImageArrayOne = new int[w1 * h1];
        ImageArrayOne = img1.getRGB(0, 0, w1, h1, ImageArrayOne, 0, w1); // 逐行扫描图像中各个像素的RGB到数组中
        int[] ImageArrayTwo = new int[w2 * h2];
        ImageArrayTwo = img2.getRGB(0, 0, w2, h2, ImageArrayTwo, 0, w2);
// 生成新图片
        BufferedImage destImage = null;

        Graphics2D g2d = null;

        if (isHorizontal) { // 水平方向合并
// DestImage = new BufferedImage(w1+w2, h1, BufferedImage.TYPE_INT_RGB);

            if (h1 >= h2) {
                destImage = new BufferedImage(w1 + w2, h1, BufferedImage.TYPE_INT_RGB);
                g2d = destImage.createGraphics();
                g2d.setPaint(Color.WHITE);
                g2d.fillRect(0, 0, w1 + w2, h1);
                g2d.dispose();
            } else {
                destImage = new BufferedImage(w2, h1, BufferedImage.TYPE_INT_RGB);//TYPE_INT_RGB
                g2d = destImage.createGraphics();
                g2d.setPaint(Color.WHITE);
                g2d.fillRect(0, 0, w2 + w1, h1);
                g2d.dispose();
            }
            destImage.setRGB(0, 0, w1, h1, ImageArrayOne, 0, w1); // 设置上半部分或左半部分的RGB
            destImage.setRGB(w1, 0, w2, h2, ImageArrayTwo, 0, w2);

        } else { // 垂直方向合并

            if (w1 >= w2) {
                destImage = new BufferedImage(w1, h1 + h2, BufferedImage.TYPE_INT_RGB);//TYPE_INT_RGB
                g2d = destImage.createGraphics();
                g2d.setPaint(Color.WHITE);
                g2d.fillRect(0, 0, w1 + w2, h1 + h2);
                g2d.dispose();
            } else {
                destImage = new BufferedImage(w2, h1 + h2, BufferedImage.TYPE_INT_RGB);//TYPE_INT_RGB
                g2d = destImage.createGraphics();
                g2d.setPaint(Color.WHITE);
                g2d.fillRect(0, 0, w2 + w1, h1 + h2);
                g2d.dispose();
            }
            destImage.setRGB(0, 0, w1, h1, ImageArrayOne, 0, w1); // 设置上半部分或左半部分的RGB
            destImage.setRGB(0, h1, w2, h2, ImageArrayTwo, 0, w2); // 设置下半部分的RGB
        }
        return destImage;
    }

    public void merge(Path srcDir, Path outFile) throws Exception {
        merge(srcDir, outFile, null);
    }

    public void merge(Path srcDir, Path outFile, Path watermark) throws Exception {

        if (Objects.isNull(srcDir)) {
            throw new FileNotFoundException("原始文件目录找不到!");
        }
        if (Objects.isNull(outFile)) {
            throw new FileNotFoundException("输出路径不能为空!");
        }


        class Img {
            BufferedImage buf;
        }

        Img distBuf = new Img();

//        AtomicInteger count = new AtomicInteger(1);

        Thumbnails.Builder<File> builder = Thumbnails.of(Objects.requireNonNull(srcDir.toFile().listFiles()))
                .scale(0.5);

        if (Objects.nonNull(watermark)) {
            builder.watermark(Positions.BOTTOM_RIGHT,
                    Thumbnails.of(ImageIO.read(watermark.toFile()))
                            .size(75, 75)
                            .asBufferedImage(), 0.8f);
        }

        builder.asBufferedImages()
                .stream()
                .peek(img -> {
                })
                .forEach(img -> {

                    if (Objects.isNull(distBuf.buf)) {
                        distBuf.buf = img;
                        return;
                    }

                    try {

                        //TODO
//                        boolean ho = count.getAndIncrement() % 4 == 0;

//                        System.out.println(count.get() + "==>" + count.getPlain());

                        distBuf.buf = mergeImage(distBuf.buf, img, false);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                });

        ImageIO.write(distBuf.buf, "png", outFile.toFile());
    }


}

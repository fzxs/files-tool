#!/bin/bash

JAVA_HOME=/home/fzxs/software/java/corretto-17

PATH=$PATH:$JAVA_HOME/bin

export PATH JAVA_HOME

# shellcheck disable=SC2164

mvn clean package -Dmaven.test.skip=true -Djava.ext.dirs=./lib

jpackage --name ppt2Img --input target --main-jar filesTool.jar --main-class com.hasnext.tool.Main --linux-shortcut -p ./lib
